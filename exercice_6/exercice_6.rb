# frozen_string_literal: true

# exercice_6.rb

# a la saisie du nom et du prenom, calculer le poids ramené sur un nombre
# de 1 chiffre en prenant en compte l'emplacement de la lettre dans l'alphabet

def change_to_num(full_name)
  # change les lettres du nom en lettre leur correspondant, un espace vaut 0
  num_name = full_name.tr('abcdefghijklmnopqrstuvwxyz ', '123456789123456789123456780')
  # je créais un array contenant chaque chiffre séparement
  number_name = num_name.to_s.split('')
  number = 0
  # pour chaque chiffre dans mon array number_name
  # ajouter chaque chiffre à number en les passant en integer
  number_name.each do |n|
    number += n.to_i
  end
  # number est maintenant égal a la somme de tous les chiffres
  # tant que number est supérieur a 9
  # séparer les chiffres de number dans un nvl array

  while number > 9
    final_nbr = 0
    split_nbr = number.to_s.split('')
    # pour chaque chiffre dans mon nvl array split_nbr
    # faire la somme de ceux ci en les passant en integer
    # si cette somme > 9 continuer sinon on a le chiffre correspondant au poid du mot
    split_nbr.each do |n|
      number = final_nbr += n.to_i
    end
  end
  number
end

# création des champs de saisie
puts 'Saisissez votre nom'
name = gets.chomp
puts 'Saisissez votre prenom'
first_name = gets.chomp

# concaténation des deux entrées
full_name = name + first_name

puts "le poid en un seul chiffre de votre prénom #{first_name} et de votre nom #{name} est de #{change_to_num(full_name)}"
