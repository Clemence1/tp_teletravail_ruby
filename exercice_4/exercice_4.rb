# frozen_string_literal: true

# exercice_4.rb

# récupère le nom
puts 'Entrer votre nom'
a = gets.chomp

# récupère l'âge
puts 'Et pourquoi pas ajouter votre age ?'
b = gets.chomp

# la méthode présentation prend 2 arguments prenom et age
def presentation(first_name, age)
  # si age est vide n'affiche que le prenom sinon affiche le prenom et l'age
  if age == ''
    "Je m'appelle #{first_name}."
  else
    "Je m'appelle #{first_name} et j'ai #{age} ans."
  end
end

puts presentation(a, b)
