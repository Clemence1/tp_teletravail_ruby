# frozen_string_literal: true
# exercice_2.rb

# création d'un tableau de nombres qui va de 1 à 100
numbers = Array(1..100)

# afficher les nombres compris entre 30 et 59
# affiche les nombres se trouvant entre l'index 29 et 58 en les affichant eux aussi
puts numbers[29..58]
puts '___'
# affiche les nombres se trouvant entre l'index 29 et 59 en excluant le dernier
puts numbers[29...59]
puts '___'
# affiche les nombres se trouvant entre l'index 29 et 58 en les affichant eux aussi
puts numbers.slice(29..58)
puts '___'
# affiche les nombres se trouvant entre l'index 29 et 58 en les affichant eux aussi
puts numbers.slice(29...59)