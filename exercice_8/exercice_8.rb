# frozen_string_literal: true

# exercice_8.rb

# depuis la saisie d'un utilisateur, convertir  la saisie pour en faire un "mot de passe" comme pour les mdp de wifi.
# exemple :  "roland moreno" deivent R0l4nd_M0r3n0"
# https://fr.wikipedia.org/wiki/Leet_speak
# Sur la page wiki vous trouverez toutes les lettres qui ont une correspondance en chiffre
