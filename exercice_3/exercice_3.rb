# frozen_string_literal: true

# exercice_3.rb

# création d'un tableau de nombres qui va de 1 à 100
numbers = Array(1..100)

# création d'un tableau qui contient les nombres paires sans methode select
even_numbers = []

# pour chaque nombre dans le tab numbers, push le nombre dans le tab even_numbers si n est pair
numbers.each do |n|
  even_numbers << n if n.even?
end

puts even_numbers.to_s

# création d'un tableau qui contient les nombres paires avec methode select
even_numbers2 = []

# push dans even_numbers2 les nombres de numbers qui sont pair
even_numbers2 << numbers.select(&:even?)

puts even_numbers2.to_s
