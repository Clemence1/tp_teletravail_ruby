# frozen_string_literal: true
# exercice_1.rb

# création d'un tableau de nombres qui va de 1 à 100
# méthode 1
numbers = Array(1..100)

# création d'un tableau de nombres qui va de 1 à 100
# méthode 2

numbers2 = []

# pour chaque nombre de 1 à 100, met la valeur de n à la suite du tableau
(1..100).each do |n|
  numbers2.push n
end

# affiche chaque valeur du tableau de nombres
puts numbers.to_s
puts numbers2.to_s
