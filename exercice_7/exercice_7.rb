# frozen_string_literal: true

# exercice_7.rb

# Construire un pendu 
# 20 mots à trouver MAX 8caractères 
# droit de jeu illimité
# MAX 6 essais else PERDU 
# SI joueur trouve mot, afficher nbre d'essais pour trouver le mot 

class Pendu 

  def initialize
    # recupere un mot aleatoire dans le tableau de mots 
    @word = words.sample
    #nombre d'essais
    @lives = 6
    # tableau qui récupère les bonnes lettres entrées par l'utilisateur
    @word_teaser = ""

     # affiche autant de trait qu'il n'y a de caractère dans le mot
    @word.first.size.times do 
      @word_teaser += "_ "
    end
  end

  # tableau contenant en premiere colonne le mot, en 2e un indice sur ce mot 
  def words
    [
      ["violet", "couleur"],
      ["rouge", "couleur"],
      ["jaune", "couleur"],
      ["cerise", "fruit"],
      ["poire", "fruit"],
      ["fraise", "fruit"],
      ["pomme", "fruit"],
      ["chien", "animal"],
      ["chat", "animal"],
      ["cheval", "animal"],
      ["vache", "animal"],
      ["pompier", "métier"],
      ["policier", "métier"],
      ["medecin", "métier"],
      ["blouson", "vêtement"],
      ["echarpe", "vêtement"],
      ["pull", "vêtement"],
      ["dragon", "animal légendaire"],
      ["licorne", "animal légendaire"],
      ["centaure", "animal légendaire"]
    ]
  end

  def print_teaser last_guess = nil
    update_teaser(last_guess) unless last_guess.nil?
    puts @word_teaser
  end

  def update_teaser last_guess
    # sépare tous les underscores dans un nouvel array 
    new_teaser = @word_teaser.split
    # 
    new_teaser.each_with_index do |letter, index|
      # remplace les valeurs vide (underscore) avec la lettre si la lettre match avec le mot
      # check si la valeur est vide (undersocre) && si la valeur qu'on vient de passer coresspond bien au mot 
      if letter == '_' && @word.first[index] == last_guess
        # remplace l'underscore avec la lettre si ça match
        new_teaser[index] = last_guess
      end
    end
    # stock le nouveau teaser et le relie avec un espace pour créer une string
    @word_teaser = new_teaser.join(' ')
  end


  def make_guess
    if @lives > 0
      #demander une lettre a l'utilisateur 
      puts "Entre une lettre"
      guess = gets.chomp

      # vérifie si la lettre entrée est incluse dans le mot
      good_guess = @word.first.include? guess

      if guess == 'exit'
        puts "Merci d'avoir joué"

      elsif guess == 'restart'
        game = Pendu.new
        game.start

      # si la lettre est contenu dans le mot, stock la lettre dans un array 
      elsif good_guess
        puts "Bonne lettre :)"

        print_teaser guess

        if @word.first == @word_teaser.split.join
          puts "Bien joué ! Tu as deviné le mot !"
          game = Pendu.new
          game.start
        else
          make_guess
        end
      else
        # si la lettre n'est pas dans le mot, perd une vie
        @lives -= 1 
        puts "Mauvaise lettre... Il te reste #{ @lives } essais. Retente ta chance :)"
        make_guess
      end
    else
      # si lives = 0; fin de partie
      puts "Fin de la partie... Retente ta chance une prochaine fois"
      game = Pendu.new
      game.start
    end
  end

  def start
    puts "NOUVELLE PARTIE"
    puts "___________________________________________________"
    puts "Pour commencer une nouvelle partie, taper 'restart'"
    puts "Pour quitter la partie, taper 'exit'"
    puts "___________________________________________________"
    puts "Le mot est long de #{ @word.first.size } caractères."
    puts "La catégorie du mot est : #{ @word.last }"
    print_teaser
  
    make_guess
  end

end

game = Pendu.new
game.start