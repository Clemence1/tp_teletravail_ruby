# tp_teletravail_ruby

exercice_1

--> Création d’un tableau avec les chiffres de 1 à 100. (2 méthodes possibles)
Une fois créer, afficher chaque valeur.

exercice_2

--> Depuis le tableau de l’exercice 1, méthode simplifiée, afficher les nombres
compris entre 30 et 59.

exercice_3

--> Toujours depuis l’exercice_1
Créer un nouveau tableau avec les nombre paires du tableau de l’exercice_1 en
utilisant la méthode « select »

exercice_4

--> Ecrire une méthode qui prendra des paramètres ‘first_name’ et ‘age’
A l’appel de la méthode, celle ci renverra :
« Je m’appelle ‘first_name’ , j’ai ‘age’ ans »
Si age n’est pas précisé la chaine renvoi
« Je m’appelle ‘first_name’ »

exercice_6 

--> À la saisie de votre nom prénom, calculer le poids ramené sur un nombre de 1
chiffre en prenant en compte l’emplacement de la lettre dans l’alphabet.

exercice_7

--> Construire un pendu.
Les mots à trouver seront de 20, il devront être maximum de 8 caractères et
pour le moment inscrit dans le code.
Le joueur a le droit de jouer de façon illimitée.
Maximum 6 essai. Si non c’est perdu.
Si le joueur trouve le mot, on lui affichera le nombre d’essai qu’il lui a fallu pour
trouver le mot du pendu.

exercice_8

--> depuis la saisie d'un utilisateur, convertir  la saisie pour en faire un "mot de passe" comme pour les mdp de wifi.
https://fr.wikipedia.org/wiki/Leet_speak
Sur la page wiki vous trouverez toutes les lettres qui ont une correspondance en chiffre

